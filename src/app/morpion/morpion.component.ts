import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-morpion',
  templateUrl: './morpion.component.html',
  styleUrls: ['./morpion.component.scss']
})
export class MorpionComponent implements OnInit {

  private message: String;
  private tableau = new Array(new Array(3), new Array(3), new Array(3));
  private gagne: boolean = false;
  private compteurX = 0;
  private compteurIA = 0;

  constructor() { }

  ngOnInit() {
  }

  jouerX(ligne: number, colonne: number) {
    if (!this.gagne) {
      this.message = null;
      if (!this.tableau[ligne][colonne]) {
        this.tableau[ligne][colonne] = 'x';
        this.compteurX++;
        this.verifierVictoire(ligne, colonne);
        if (!this.gagne && (this.compteurX + this.compteurIA < 9)) {
          setTimeout(() => { this.jouerIA(); }, 100);
        }
      } else {
        this.message = "Cette case a déjà été jouée.";
      }
    } else {
      this.reinitialiser();
    }
  }

  chercherCellulesVides() {
    let cellulesVides = [];
    for (let i=0; i<this.tableau.length; i++) {
      for (let j=0; j<this.tableau[i].length; j++) {
        if (!this.tableau[i][j]) {
          cellulesVides.push([i,j]);
        }
      }
    }
    return cellulesVides;
  }

  jouerIA() {
      let choixAleatoire = this.chercherCellulesVides()[this.aleatoire()];
      this.tableau[choixAleatoire[0]][choixAleatoire[1]] = "o";
      this.compteurIA++;
      this.verifierVictoire(choixAleatoire[0], choixAleatoire[1]);
  }

  aleatoire() {
    return Math.floor(Math.random() * Math.floor(this.chercherCellulesVides().length));
  }

  verifierVictoire(ligne: number, colonne: number) {
    //vérification par rapport au dernier coup
    if (this.compteurX > 2 || this.compteurIA > 2) {
      let diagonale1 = [];
      let diagonale2 = [];
      //Première diagonale
      for (let i=0, j=0; i<this.tableau.length; i++, j++) {
        if (this.tableau[i][j]) {
          diagonale1.push(this.tableau[i][j]);
        }
      }
      if (diagonale1[0] === diagonale1[1] && diagonale1[1] === diagonale1[2]) {
        this.gagner(diagonale1[0]);
      } else {
        //Deuxième diagonale
        for (let i=0, j=2; i<this.tableau.length; i++, j--) {
          if (this.tableau[i][j]) {
            diagonale2.push(this.tableau[i][j]);
          }
        }
        if (diagonale2[0] === diagonale2[1] && diagonale2[1] === diagonale2[2]) {
          this.gagner(diagonale2[0]);
          //Dernière ligne jouée
        } else if (this.tableau[ligne][0] === this.tableau[ligne][1] && this.tableau[ligne][1] === this.tableau[ligne][2]) {
          this.gagner(this.tableau[ligne][0]);
          //Dernière colonne jouée
        } else if (this.tableau[0][colonne] === this.tableau[1][colonne] && this.tableau[1][colonne] === this.tableau[2][colonne]) {
          this.gagner(this.tableau[0][colonne]);
        } else if (this.compteurX + this.compteurIA === 9) {
          this.message = "Match nul !"
          this.gagne = true;
        }
      }
    }
  }

  gagner(gagnant: String) {
    this.message = this.message = "Les " + gagnant + " ont remporté la partie !"
    this.gagne = true;
    if (gagnant === 'x') {
      for (let i=0; i<this.tableau.length; i++) {
        for (let j=0; j<this.tableau[i].length; j++) {
          if (j === 0) {
            setTimeout(() => { this.tableau[i][j] = 'W'; }, 400);
          } else if (j === 1) {
            setTimeout(() => { this.tableau[i][j] = 'I'; }, 500);
          } else if (j === 2) {
            setTimeout(() => { this.tableau[i][j] = 'N'; }, 600);
          }
        }
      }
    }
  }

  reinitialiser() {
    this.tableau = new Array(new Array(3), new Array(3), new Array(3));
    this.message = null;
    this.gagne = false;
    this.compteurX = 0;
    this.compteurIA = 0;
  }

}

